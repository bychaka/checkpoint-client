import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';


import { EmployeesApiService } from '../../services/spgApiService';
import { setEmployeeSummaryTime, setCurrentEmployeeId, setCurrentEmployeeDate } from '../../actions/EmployeesPageActions';
import MomentUtils from '@date-io/moment';
import moment from "moment";
import { connect } from 'react-redux';
import React from 'react';

const styles = theme => ({
  container: {
    margin: '0 auto',
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
  },
  textField: {
    width: 300,
  },
  formItem: {
    marginBottom: 40,
  }
});

class TimeForm extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      selectedDate: '',
      open: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    this.getSummarylTime();
  }

  handleChange = event => {
    this.props.setCurrentEmployeeId({ currentEmployeeId: event.target.value});
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  async getSummarylTime(){
    try {
      const time = await EmployeesApiService.getEmployeeSummaryTime(this.props.currentEmployeeId, this.props.selectedDate);
      this.props.setEmployeeSummaryTime({ summaryTime: time.totalTime });
    }
    catch (err) {
      console.error(err);
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    console.log(this.state);
    this.getSummarylTime();
  };

  render() {
    const { classes } = this.props;
    console.log(this.props);
    console.log(this.props.selectedDate);
    return (
      <form className={classes.container} noValidate autoComplete="off" onSubmit={this.handleSubmit}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="controlled-select">Employee</InputLabel>
          <Select
            className = {classes.formItem}
            open={this.state.open}
            onClose={this.handleClose}
            onOpen={this.handleOpen}
            value={this.props.currentEmployeeId}
            onChange={this.handleChange}
            inputProps={{
              name: 'employeeId',
              id: 'controlled-select',
            }}
          >
            {this.props.data.map((option, index) => (
              <MenuItem key={index} value={option.EmployeeID}  >
                {option.FIO}
              </MenuItem>
            ))}
          </Select>        
        </FormControl>

        <MuiPickersUtilsProvider utils={MomentUtils} moment={moment}>
          <DatePicker
          onChange={(date) => this.props.setCurrentEmployeeDate({ currentEmployeeDate: moment(new Date(date)).format('YYYY-MM-DD')})}
          format = "YYYY-MM-DD"
          id="date"
          label="Select date"
          value={this.props.selectedDate}
          className={[classes.textField, classes.formItem].join(' ')}
          InputLabelProps={{
            shrink: true,
          }}
          />
        </MuiPickersUtilsProvider>

        <Typography variant="h6" gutterBottom className = {classes.formItem}>
          Working time per day - {this.props.summaryTime}.
        </Typography>

        <Button type="submit" variant="outlined" color="primary" className={classes.button}>
        Get total time
        </Button>
      </form>
    );
  }
}

TimeForm.propTypes = {
   classes: PropTypes.object.isRequired,
};

const mapStateToProps = store => {
  return {
    summaryTime: store.employeeTime.summaryTime,
    selectedDate: store.employeeTime.currentEmployeeDate,
    currentEmployeeId: store.employeeTime.currentEmployeeId,
  };
};

const mapDispatchToProps = dispatch => ({
  setEmployeeSummaryTime: summaryTime => dispatch(setEmployeeSummaryTime(summaryTime)),
  setCurrentEmployeeDate: currentDate => dispatch(setCurrentEmployeeDate(currentDate)),
  setCurrentEmployeeId: currentEmployeeId => dispatch(setCurrentEmployeeId(currentEmployeeId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(TimeForm));