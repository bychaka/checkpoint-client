import React, { Component } from 'react';
import './EmployeesList.css';
import { connect } from 'react-redux';

import Loader from '../Loader/Loader';
import EmployeesTable from '../EmployeesTable/EmployeesTable';
import { EmployeesApiService } from '../../services/spgApiService';
import { setEmployees} from '../../actions/EmployeesPageActions';

class EmployeesList extends Component {
  isLoading = true;

  loadEmployees(){
    this.isLoading = true;
    EmployeesApiService.getEmployeesList()
      .then(employeesList => {
        this.isLoading = false;
        this.props.setEmployees({ employees: employeesList });
      })
      .catch(err => {
        console.error(err);
      });
  }

  componentDidMount() {
    
    //load imitation
    setTimeout(() => {
      this.loadEmployees();
    }, 800);

    this.intervalId = setInterval(this.loadEmployees.bind(this), 60000);    
  }

  componentWillUnmount(){
    clearInterval(this.intervalId);
  }

  render() {
    console.log(this.isLoading);
    if (this.isLoading) {
      return <Loader />;
    }

    return (
      <React.Fragment>
        <div className="employee-list">
          <EmployeesTable  data={this.props.employees}/>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = store => {
  return {
    employees: store.employeesList.employees,
    current: store.employeeTime,
  };
};

const mapDispatchToProps = dispatch => ({
  setEmployees: employees => dispatch(setEmployees(employees)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeesList);
