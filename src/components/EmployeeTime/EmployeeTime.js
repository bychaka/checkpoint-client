import React from 'react';
import { connect } from 'react-redux';
import { EmployeesApiService } from '../../services/spgApiService';
import { setEmployees } from '../../actions/EmployeesPageActions';
import TimeForm from '../TimeForm/TimeForm'
import './EmployeeTime.css';

class EmployeeTime extends React.Component {
  
  componentDidMount() {
    EmployeesApiService.getEmployeesList()
      .then(employeesList => {
        this.isLoading = false;
        this.props.setEmployees({ employees: employeesList });
      })
      .catch(err => {
        console.error(err);
      });
  }

  render() { 
    return (
      <div className="form-wrapper">
        <TimeForm data={this.props.employees} />
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    employees: store.employeesList.employees,
  };
};

const mapDispatchToProps = dispatch => ({
  setEmployees: employees => dispatch(setEmployees(employees)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeTime);