export const setEmployees = employees => {
  return {
    type: 'SET_EMPLOYEES_LIST',
    payload: employees,
  };
};

export const setEmployeeSummaryTime = totalTime => {
  return {
    type: 'SET_EMPLOYEE_SUMMARY_TIME',
    payload: totalTime,
  };
};

export const setCurrentEmployeeId = currentEmployeeId => {
  return {
    type: 'SET_CURRENT_EMPLOYEE_ID',
    payload: currentEmployeeId,
  };
};

export const setCurrentEmployeeDate = currentEmployeeDate => {
  return {
    type: 'SET_CURRENT_EMPLOYEE_DATE',
    payload: currentEmployeeDate,
  };
};