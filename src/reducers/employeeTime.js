export const initialState = {
  summaryTime: '',
  currentEmployeeId: '',
  currentEmployeeDate: '',
};

export const employeeTimeReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_EMPLOYEE_SUMMARY_TIME':
      return Object.assign({}, state, action.payload);
    case 'SET_CURRENT_EMPLOYEE_ID':
      return Object.assign({}, state, action.payload);
    case 'SET_CURRENT_EMPLOYEE_DATE':
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
};
