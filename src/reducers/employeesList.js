export const initialState = {
  employees: [],
};

export const employeesListReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_EMPLOYEES_LIST':
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
};
