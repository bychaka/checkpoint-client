import * as http from 'axios';
import { spgApiEndpoints } from './spgApi';

export class EmployeesApiService {
  static async getEmployeesList() {
    try {
      const fetched = await http.get(spgApiEndpoints.employeesList);
      // const response = fetched.data;
      return fetched.data;
    } catch (err) {
      console.error(`Error during fetching data of employeesList: ${err.message}`);
      return [];
    }
  }

  static async getEmployeeSummaryTime(employeeId, date){
    try{
      const fetched = await http.get(
        spgApiEndpoints.employeeSummaryTime.replace(':employeeId', employeeId).replace(':date', date)
      );
      return fetched.data;
    } catch (err) {
      console.error(`Error during fetching data of employeesSummaryTime: ${err.message}`);
      return [];
    }
  }

}
