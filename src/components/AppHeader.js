import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './AppHeader.css';

class AppHeader extends Component {
  render() {
    return (
      <div className="header">
        <div className="menu">
          <Link to="/" className="title-link">
        <h6>Employees List</h6>
        </Link>
        <Link to="/employees/time" className="title-link">
        <h6>Employee summary time</h6>
        </Link>
        </div>
        <h2 className="title">SPGAcademy API application</h2>
      </div>
    );
  }
}

export default AppHeader;
