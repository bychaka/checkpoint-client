export const spgApiEndpoints = {
  employeesList: 'http://localhost:3000/api/v1/employees',
  employeeSummaryTime: 'http://localhost:3000/api/v1/employees/:employeeId/summary/:date',
};
