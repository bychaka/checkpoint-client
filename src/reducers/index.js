import { combineReducers } from 'redux';
import { employeesListReducer } from './employeesList';
import { employeeTimeReducer } from './employeeTime';

export const rootReducer = combineReducers({
  employeesList: employeesListReducer,
  employeeTime: employeeTimeReducer,
});
