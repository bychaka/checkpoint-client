import React, { Component } from 'react';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { setCurrentEmployeeId, setCurrentEmployeeDate } from '../../actions/EmployeesPageActions';
import moment from "moment";

import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 350,
  },
  tableRow: {
    textDecoration: 'none',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#fffdef',
    },
  },
});

class EmployeeTableRow extends Component {
  state = {
    redirect: false
  }

  static contextTypes = {
    router: PropTypes.object
  }
  redirectToTarget = () => {
    this.context.router.push(`/`)
  }

  getStatus(eventId){
    switch(eventId){
      case 45:
        return(
          <span className="attend">Attend</span>
        );
      case 46:
          return(
          <span className="absent">Absent</span>
          );
      default:
          return("unknown " + eventId);
    }
  }


  handleOnClick = (e) => {
    e.stopPropagation();
    console.log(e);
    console.log(this.props.data);
    this.setState({ redirect: true })
    this.props.setCurrentEmployeeId({ currentEmployeeId: this.props.data.EmployeeID});
    this.props.setCurrentEmployeeDate({ currentEmployeeDate: moment(this.props.data.Date).format('YYYY-MM-DD')});
  }


  render() {
        const { redirect } = this.state;

     if (redirect) {
       return <Redirect to='/employees/time'/>;
     }
    const employeeId = this.props.data.EmployeeID;
    const employee = this.props.data.FIO;
    const status = this.props.data.EventDescriptionID;
    const date = this.props.data.Date;

    const { classes } = this.props;


    return (
        <TableRow 
        className={classes.tableRow}
        onClick={this.handleOnClick}
        >
            <TableCell className="table-id" component="th" scope="row">{employeeId}</TableCell>
            <TableCell className="table-employee">{employee}</TableCell>
            <TableCell className="table-status">{this.getStatus(status)}</TableCell>
            <TableCell className="table-time">{date}</TableCell>
        </TableRow>
    );
  }
}

const mapStateToProps = store => {
  return {
    currentDate: store.employeeTime.currentEmployeeDate,
    currentEmployeeId: store.employeeTime.currentEmployeeId,
  };
};

const mapDispatchToProps = dispatch => ({
  setCurrentEmployeeDate: currentDate => dispatch(setCurrentEmployeeDate(currentDate)),
  setCurrentEmployeeId: currentEmployeeId => dispatch(setCurrentEmployeeId(currentEmployeeId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(EmployeeTableRow));