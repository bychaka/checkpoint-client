import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


import classNames from 'classnames';


import EmployeeTableRow from '../EmployeesTableRow/EmployeesTableRow';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 350,
  },
  tableRow: {
    cursor: 'pointer',
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
});

class SimpleTable extends Component {
  
  getRowClassName = ({ index }) => {
    const { classes, rowClassName, onRowClick } = this.props;

    return classNames(classes.tableRow, rowClassName, {
      [classes.tableRowHover]: index !== -1 && onRowClick != null,
    });
  };

  render(){
    const { classes } = this.props;

    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell className="table-head" style={{width:'100'}} >ID</TableCell>
              <TableCell className="table-head">Employee</TableCell>
              <TableCell className="table-head">State</TableCell>
              <TableCell className="table-head">State change time</TableCell>
            </TableRow>
          </TableHead>
          <TableBody className="table-body">
            {this.props.data.map((row, index) => (
              <EmployeeTableRow  key={index} data={row} />
            ))}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

SimpleTable.propTypes = {
  classes: PropTypes.object.isRequired,
  onRowClick: PropTypes.func,
  rowClassName: PropTypes.string,
};

export default withStyles(styles)(SimpleTable);