import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import EmployeesList from './components/EmployeeList/EmployeesList';
import EmployeeTime from './components/EmployeeTime/EmployeeTime';
// import NotFound from './components/NotFound/NotFound';

export class AppRouter extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/employees" component={EmployeesList} />
        <Route path="/employees/time" component={EmployeeTime} />
        {/* <Route path="*" component={NotFound} /> */}
      </Switch>
    );
  }
}
